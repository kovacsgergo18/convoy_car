#!/usr/bin/env python

import time

from . import communication
from . import messages

import rospy

from convoy_common.msg import ControlCommand, Status, ManagementCommand


class CarController(object):
    MODE_COMMON_CONTROL = 'COMMON_CNTRL'
    MODE_INDIVIDUAL_CONTROL = 'INDIVIDUAL_CNTRL'
    MODES = [MODE_COMMON_CONTROL, MODE_INDIVIDUAL_CONTROL]

    def __init__(self, car_id):
        self.car_id = car_id
        self._cntrl_subscriber = None
        self._mode = None
        self._command = messages.Command()
        self.previous_battery_voltage = 0.0

        self.teleop_control_topic_name = "convoy_car_{}_teleop".format(self.car_id)
        self.convoy_control_topic_name = "convoy_car_control"
        self.status_topic_name = "convoy_car_status"
        self.management_topic_name = 'convoy_car_{}_mgmt'.format(self.car_id)

    def run(self):
        rospy.init_node('convoy_car_{}'.format(self.car_id))
        self._cntrl_subscriber = rospy.Subscriber(self.convoy_control_topic_name, ControlCommand, self.cntrl_callback)
        self.mgmt_subscriber = rospy.Subscriber(self.management_topic_name, ManagementCommand, self.mgmt_callback)
        self.status_pub = rospy.Publisher(self.status_topic_name, Status, queue_size=10)
        rospy.init_node('convoy_car_{}'.format(self.car_id))

        self.comm = self._get_communicator()
        print(self.comm)

        with self.comm:
            rospy.spin()

    def _get_communicator(self):
        try:
            import spidev

            return communication.SPI()
        except ImportError:
            return communication.Dummy()

    def mgmt_callback(self, message):
        print(message)
        if message.mode != self._mode:
            if message.mode.lower() == 'teleop':
                self.change_cntrl_subscriber(self.teleop_control_topic_name)
            elif message.mode.lower() == 'convoy':
                self.change_cntrl_subscriber(self.convoy_control_topic_name)

    def change_cntrl_subscriber(self, topic_name):
        if self._cntrl_subscriber:
            self._cntrl_subscriber.unregister()

        self._cntrl_subscriber = rospy.Subscriber(
            topic_name,
            ControlCommand,
            self.cntrl_callback
        )

    def cntrl_callback(self, message):
        self._command.v_max_tcl = message.v_max
        self._command.d_ref_tcl = message.d_ref
        self._command.d_tcl = message.d_tcl
        self.update_car()

    def update_car(self):
        status = messages.Status()
        response = self.comm.send(self._command.to_byte_array())
        status.from_byte_array(response)
        status_msg = Status(
            x = status.x,
            y = status.y,
            theta = status.theta,
            battery=status.bat,
            battery_filtered=self.filter_battery_voltage(status.bat),
            mode=self._mode,
            hw_communication_adapter=self.comm.name
        )
        self.status_pub.publish(status_msg)
        print(status_msg.x)
    def filter_battery_voltage(self, battery_voltage):
        alpha = 0.025
        if self.previous_battery_voltage != 0.0:
            filtered_voltage = battery_voltage * alpha + (1.0 - alpha) * self.previous_battery_voltage
        else:
            filtered_voltage = battery_voltage

        self.previous_battery_voltage = filtered_voltage
        return filtered_voltage
