
#!/usr/bin/env python

import struct


class AbstractMessage(object):
    def to_byte_array(self):
        pass

    def from_byte_array(self, data):
        pass

    def float_to_byte_array(self, value):
        packed = struct.pack('f', value)
        return [ord(packed[0]), ord(packed[1]), ord(packed[2]), ord(packed[3])]

    def int_to_byte_array(self, value):
        packed = struct.pack('i', value)
        return [ord(packed[0]), ord(packed[1]), ord(packed[2]), ord(packed[3])]

    def byte_array_to_float(self, byte_array):
        b2 = bytes()
        b2 += chr(byte_array[0])
        b2 += chr(byte_array[1])
        b2 += chr(byte_array[2])
        b2 += chr(byte_array[3])
        return struct.unpack('f', ''.join(b2))[0]

    def byte_array_to_int(self, byte_array):
        b2 = bytes()
        b2 += chr(byte_array[0])
        b2 += chr(byte_array[1])
        b2 += chr(byte_array[2])
        b2 += chr(byte_array[3])
        return struct.unpack('i', ''.join(b2))[0]

    def calculate_checksum(self, data):
        return sum(data)


class Command(AbstractMessage):
    MODE_AUTONOMOUS = 0
    MODE_COORDINATED = 1

    v_max_tcl = 0.0
    d_ref_tcl = 0.0
    d_tcl = 0.5
    mode = MODE_COORDINATED
    debug_len = 0
    debug_cmd = []

    def to_byte_array(self):
        data = []
        data.extend(self.float_to_byte_array(self.v_max_tcl))
        data.extend(self.float_to_byte_array(self.d_ref_tcl))
        data.extend(self.float_to_byte_array(self.d_tcl))
        data.extend(self.int_to_byte_array(self.mode))
        data.extend(self.int_to_byte_array(self.debug_len))
        for i in xrange(20):
            data.extend(self.int_to_byte_array(0))
        data.extend(self.int_to_byte_array(self.calculate_checksum(data)))
        print('v_max_tcl: {}'.format(self.v_max_tcl))
        print('d_ref_tcl: {}'.format(self.d_ref_tcl))
        print('d_tcl: {}'.format(self.d_tcl))
        print('mode: {}'.format(self.mode))
        return data


class Status(AbstractMessage):
    x = 0.0,
    y = 0.0,
    theta = 0.0,
    bat = 0.0,

    def from_byte_array(self, data):
        if len(data) != 104:
            print('Invalid length: {}'.format(len(data)))
            return

        self.x =self.byte_array_to_float(data[0:4])
        self.y = self.byte_array_to_float(data[4:8])
        self.theta = self.byte_array_to_float(data[8:12])
        self.bat = self.byte_array_to_float(data[12:16])
