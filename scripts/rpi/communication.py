#!/usr/bin/env python

import logging
from .messages import Command


class SPI(object):
    @property
    def name(self):
        return 'SPIAdapter'

    def __enter__(self):
        import spidev

        self._spi = spidev.SpiDev()
        self._spi.open(0, 0)

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._spi.xfer2(Command().to_byte_array())
        self._spi.close()

    def send(self, data):
       return self._spi.xfer2(data)


class Dummy(object):
    @property
    def name(self):
        return 'DummyAdapter'

    def __enter__(self):
        logging.debug('__enter__ called')

    def __exit__(self, exc_type, exc_val, exc_tb):
        logging.debug('__exit__ called')

    def send(self, data):
       return [0x00 for _ in xrange(104)]
