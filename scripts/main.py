#!/usr/bin/env python

import sys

import rospy

from rpi.application import CarController


if __name__ == '__main__':
    try:
        app = CarController(sys.argv[1])
        app.run()

        print('exiting')
    except rospy.ROSInterruptException:
        print('exiting')
    except KeyboardInterrupt:
        print('exiting on keyboard interrupt')
